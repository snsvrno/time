import ansi.Paint.paint;
import ansi.Paint.paintPreserve;
import ansi.colors.Style;

import tui.Out.error as err;
import tui.Out.debug as dbg;
import tui.Out.warning as wrn;

import ansi.Format.color as c;