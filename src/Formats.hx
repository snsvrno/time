package;

class Formats {
	public static function norm(string:String) : String {
		return confPaint(string, "normal");
		/*
		return paintPreserve(
			string,
			parseColor("styles_normal_foreground"), 
			//parseColor(Config.styles_normal_background), 
			Dim
		);*/
	}
	
	public static function tgs(ts:Array<String>) : String {
		return [ for (t in ts) "#" + paint(t, Blue) ].join(", ");
	}

	public static function ann(annotation:String) : String {
		return paint(annotation, Green);
	}

	public static function dur(duration:Float) : String {
		return paint(Math.floor(duration * 100)/100, Magenta);
	}

	private static function parseColor(name:Null<String>) : Null<ansi.colors.Color> {
		if (name == null) return null;
		else return switch(name.toLowerCase()) {
			case "white": White;
			case "blue": Blue;
			case "green": Green;
			case "magenta": Magenta;
			default: trace('unknown color $name'); null;
		}
	}

	private static function confPaint(text:String, name:String) : String {
		return paintPreserve(
			text,
			parseColor(Reflect.getProperty(Config, "styles_" + name + "_foreground")),
			parseColor(Reflect.getProperty(Config, "styles_" + name + "_background")),
		);
	}
}
