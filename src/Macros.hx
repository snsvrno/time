class Macros {

	public static function build(path:String) {
		var fields = haxe.macro.Context.getBuildFields();

		var content = sys.io.File.getContent(path);
		var data = haxe.Json.parse(content);

		for (key in Reflect.fields(data)) {
			var value = Reflect.getProperty(data, key);
			fields.push({
				name: key,
				access: [APublic, AStatic],
				kind: FVar(null, macro $v{value}),
				pos: haxe.macro.Context.currentPos(),
			});		
		}

		return fields;
	}
}