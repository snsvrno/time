package commands;

import Formats.*;

class Edit implements tui.types.Command {

	public var description : String = Strings.cmd.edit.desc;

	public function run(... args:String) {
		var editor = new sys.io.Process("notepad.exe");
		var code = editor.exitCode(true);
		editor.close();
		trace('done');
	}

	public function check(name:String) : Bool {
		return name == "edit" || name == "e";
	}
}
