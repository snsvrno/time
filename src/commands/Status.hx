package commands;

import Formats.*;

class Status implements tui.types.Command {

	public var description : String = Strings.cmd.status.desc;

	public function run(... args:String) {
		var current = ds.Track.current();
		if (current == null) Sys.println(norm(Strings.general.no_tracking));
		else {
			Sys.println(norm('${Strings.general.already_tracking}: $current'));
		}
	}
}
