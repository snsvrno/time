package commands;

import Formats.*;

class Start implements tui.types.Command {

	public var description : String = Strings.cmd.start.desc;

	public var switches : Array<tui.types.Switch> = [
		{ name: "tag", short: "-t", description: Strings.cmd.start.switches.tag, value: true },
		{ name: "annotation", short: "-a", description: Strings.cmd.start.switches.ann, value: true },
	];

	public function run(... args:String) {
		var current = ds.Track.current();

		if (current != null) {
			Sys.println(norm('${Strings.general.already_tracking}: $current'));
			return;
		}

		var track = new ds.Track();
		track.start = Date.now();
		track.tags = tui.Switches.getValueOr("tag","").split(',');
		if (track.tags.length == 1 && track.tags[0] == "") track.tags = [];
		track.annotation = tui.Switches.getValueOr("annotation","");
		track.save();

		Sys.println(norm('${Strings.general.started_tracking}: $track'));
	}
}
