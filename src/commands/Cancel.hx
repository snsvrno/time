package commands;

import Formats.*;

class Cancel implements tui.types.Command {

	public var description : String = Strings.cmd.resume.desc;

	public function run(... args:String) {
		var current = ds.Track.current();
		if (current == null) {
			Sys.println(norm('${Strings.general.no_tracking}'));
		}
		else {
			current.remove();
			Sys.println(norm('${Strings.cmd.cancel.cancelled}: $current'));
		}

	}
}
