package commands;

import ansi.CommandTools;
import Formats.*;

using StringTools;

class List implements tui.types.Command {

	public var description : String = Strings.cmd.list.desc;

	public var switches : Array<tui.types.Switch> = [
		{ name: "tag", short: "-t", description: Strings.cmd.list.switches.tag, value: true },
		{ name: "annotation", short: "-a", description: Strings.cmd.list.switches.ann, value: true },
		{ name: "display", short: "-d", description: Strings.cmd.list.switches.display, value: true },
		{ name: "totalize", short: "-q", description: Strings.cmd.list.switches.totalize, value: true },
	];


	public function run(... args:String) {
		var query = ds.Query.create();

		
		// filtering by tags
		for (tag in tui.Switches.getValuesOr("tag", [])) {
			if (tag.charAt(0) == "-") {
				tag = tag.substring(1);
				query.removeTag(tag);
			} else {
				if (tag.charAt(0) == "+") tag = tag.substring(1);
				query.keepTag(tag);
			}
		}

		var individual = tui.Switches.getValue("display");
		var tots = tui.Switches.getValue("totalize");
		if (individual == null && tots == null) normalDisplay(query.items);
		else if (individual != null) {
			for (item in query.items) {
				Sys.println(norm(item.format(individual)));
			}
		} else if (tots != null) {
			totalize(query.items, tots);
		}

	}

	public function check(name:String) : Bool {
		return name == "list" || name == "ls";
	}

	private static function totalize(items:Array<ds.Track>, command:String) {
		var regex = new EReg("\\%([A-Za-z]+)\\(([A-Za-z]+)\\)\\%", "g");

		while(regex.match(command)) {
			var value = "";

			switch(regex.matched(1)) {
				case "sum":
					var total:Float = 0;
					for (item in items) {
						var f = item.format('%${regex.matched(2)}%', true);
						var v = Std.parseFloat(f);
						if (Math.isNaN(v)) throw 'error running $command';
						total += v;
					}

					total = Math.floor(total*100)/100;
					value = dur(total);

				case other:
					throw 'unknown totalizer command $other';
			}

			command = command.replace(regex.matched(0), value);
		}

		Sys.println(norm(command));
	}

	private static function normalDisplay(items:Array<ds.Track>) {
		var lastDay = "";
		for (item in items) {
			var day = DateTools.format(item.start, Config.date_format);

			var painter = if (day != lastDay) (text) -> paint(text, White);
			else (text) -> paint(text, Black, FGBright);

			lastDay = day;
			day = painter(day);

			var start = painter(DateTools.format(item.start, Config.time_format));
			var end = if (item.end == null) norm(" ... ");
			else painter(DateTools.format(item.end, Config.time_format));

			var tags = if (item.tags.length > 0) " " + tgs(item.tags) else "";
			var annotation = if (item.annotation != null) " " + ann(item.annotation) else "";

			Sys.println('$day  $start ${paint("->", Black, FGBright)} $end $tags$annotation');
		}
	}
}
