package commands;

import Formats.*;

class Stop implements tui.types.Command {

	public var description : String = "stops tracking";

	public function run(... args:String) {
		var current = ds.Track.current();

		if (current == null) {
			Sys.println(norm(Strings.general.no_tracking));
			return;
		}

		current.remove();
		current.end = Date.now();
		current.save();

		Sys.println(norm('${Strings.general.finished}: $current'));
	}
}
