package commands;

import Formats.*;

class Resume implements tui.types.Command {

	public var description : String = Strings.cmd.resume.desc;

	public function run(... args:String) {
		var current = ds.Track.current();
		if (current == null) {
			var recent = ds.Query.create().items[0];
			if (recent == null) {
				Sys.println(norm(Strings.cmd.resume.nothing_to_resume));
				return;
			}

			var track = new ds.Track();
			track.start = Date.now();
			track.tags = recent.tags.copy();
			track.annotation = recent.annotation;
			track.save();
	
			Sys.println(norm('${Strings.general.started_tracking}: $track'));
		}
		else {
			Sys.println(norm('${Strings.general.already_tracking}: $current'));
		}

	}
}
