package;

inline var ROOT_DIR:String = ".timings";
inline var CONF_NAME:String = "time.toml";

@:build(tui.macros.Config.load("res/default.toml"))
class Config {
	
	inline public static function getFolder() : String {
		return haxe.io.Path.join([
			sn.Fs.userDirectory(),
			ROOT_DIR
		]);
	}
	
	inline public static function getPath() : String {
		return haxe.io.Path.join([getFolder(), CONF_NAME]);
	}

	public static function parse(content:String) : Dynamic {
		var data = {};
		var comment = "";
		var header = "";
	
		for (line in sn.ds.StringTools.lines(content)) {
			if (StringTools.trim(line).length == 0) continue;
	
			if (line.charAt(0) == "#") {
		
				if (comment.length > 0) comment += " " + StringTools.trim(line.substring(1));
				else comment = line.substring(1);

			} else if (line.charAt(0) == "[") {

				header = line.substring(1, line.length-2).split(".").join("_") + "_";

			} else {

				var split = line.split("#");
				if (split[1] != null) comment = split[1];
				split = split[0].split("=");
				var key = header + StringTools.trim(split[0]);
				var value = StringTools.trim(split[1]);
				if (comment.length > 0) Reflect.setField(data, "___" + key, comment);
				var val:Any = {
					if (value.charAt(0) == "\"") value.substring(1,value.length-1);
					else if (value.toLowerCase() == "true") true;
					else if (value.toLowerCase() == "false") false;
					else if (Std.parseInt(value) != null) Std.parseInt(value);
					else if (Std.parseFloat(value) != Math.NaN) Std.parseFloat(value);
					else { trace('cannot parse $value'); null; }
				};
				Reflect.setField(data, key, val);
				comment = "";
			
			}
		}
		return data;
	}

	public static function stringify(data:Dynamic) : String {
		var content = "";
		for (key in Reflect.fields(data)) {
			var value = Reflect.getProperty(data, key);
			if (value is String) value = '"$value"';
			content += '$key = $value\n';
		}
		return content;
	}
}