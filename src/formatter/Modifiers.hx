package formatter;

typedef Modifier = (v:Dynamic, ... args:String) -> String;
typedef Modifiers = Map<String, Modifier>;
