package formatter;

enum Values {
	Value(value:Dynamic);
	ValueMod(value:Dynamic, mods:Modifiers);
	Func(func:() -> String);
}
