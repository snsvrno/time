package formatter;

class Formatter {

	inline private static var MAINTOKEN:String = "\\$";
	inline private static var MODTOKEN:String = "\\:";

	private static var regex:EReg = new EReg('$MAINTOKEN([A-Za-z]*)($MODTOKEN([^\\$]*))?$MAINTOKEN', "");

	public static function run(text:String, values:Map<String, Values>) : String {
		while(regex.match(text)) text = regex.map(text, (r:EReg) -> process(r, values));
		return text;
	}

	private static function process(reg:EReg, values:Map<String, Values>) : String {
		var name = reg.matched(1);
		switch(values.get(name)) {
			case Func(f):
				return f();

			case Value(val):
				return '$val';

			case ValueMod(val, mods):
				var options = reg.matched(3);
				if (options == null) return '$val';
				else {
					var opts = options.split(",");
					var fname = opts.shift();
					var f = mods.get(fname);
					if (f != null) return f(val, ... opts);
					else Sys.println(fname + " does not exist");
				}
				return '$val';

			default:
				trace('nothing found for $name');
				return "null";
		}
		return "<!ERROR!>";

	}

}
