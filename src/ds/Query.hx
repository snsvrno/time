package ds;

class Query {

	public var items:Array<Track> = [];

	private function new() { }

	public static function create() : Query {
		var query = new Query();

		var rootFolder = ds.Track.getFolder();

		if (!sys.FileSystem.exists(rootFolder)) return query;

		for (folder in sys.FileSystem.readDirectory(rootFolder)) {
			var subfolder = haxe.io.Path.join([rootFolder, folder]);
			if (sys.FileSystem.isDirectory(subfolder)) {
				dbg(c('looking in folder $subfolder'));
				for (item in sys.FileSystem.readDirectory(subfolder)) {
					var track = ds.Track.load(haxe.io.Path.join([subfolder, item]));
					query.items.push(track);
				}
			}
		}

		// sort from oldest to newest
		query.items.sort((a,b) -> if (a.start.getTime() > b.start.getTime()) return 1 else return -1);

		return query;
	}

	public function removeTag(tag:String) {
		var pos = items.length-1;
		while(pos >= 0) {
			var item = items[pos];
			if (item.hasTag(tag)) items.remove(item);
			pos -= 1;
		}
	}

	public function keepTag(tag:String) {
		var pos = items.length-1;
		while(pos >= 0) {
			var item = items[pos];
			if (!item.hasTag(tag)) items.remove(item);
			pos -= 1;
		}
	}

}
