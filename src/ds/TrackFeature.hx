package ds;

enum abstract TrackFeature(String) to String{
	var Start = "s";
	var End = "e";
	var Annotation = "a";
	var Tag = "t";
}

var Features:Array<TrackFeature> = [ Start, End, Annotation, Tag ];
