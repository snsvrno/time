package ds;

using StringTools;
using sn.ds.StringTools;

class Version {
	// loads the version number in the file and builds it with it
	public static macro function load(file:String) : haxe.macro.Expr {
		var array = sys.io.File.getContent(file);
		return macro $v{array};
	}

	public static macro function fromGit() : haxe.macro.Expr {
		var currentTag = {
			var process = new sys.io.Process("git", ["describe", "--tags"]);
			var tag = process.stdout.readAll().toString();
			tag.trim().replace("\r","").replace("\n","");
		};

		var modified = {
			var dirty = false;
			var process = new sys.io.Process("git", ["status", "-s"]);
			var text = process.stdout.readAll().toString();
			var files = text.lines();
			for (m in files) {
				if (m.trim().split(" ")[0] == "M") dirty = true;
			}
			dirty;
		}

		var ver = currentTag;
		if (ver == "") ver = "(untracked)";
		if (modified) ver += "+";

		return macro $v{ver};
	}
}
