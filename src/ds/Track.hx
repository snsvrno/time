package ds;

import Formats.*;

using StringTools;

inline var ROOT_DIR:String = "tracks";
// must be FS approve / safe
inline var DATE_FORMAT:String = "%Y%m%d%H%M%S";
inline var SUB_FOLDER_FORMAT:String = "%Y-%m";

class Track {

	public var tags:Array<String> = [];
	public var annotation:String;
	public var start:Date;
	public var end:Null<Date>;

	public function new() { }

	inline static public function getFolder() : String {
		return haxe.io.Path.join([
			Config.getFolder(),
			ROOT_DIR
		]);
	}

	public static function load(path:String) : Track {
		dbg(c('loading track data $path'));
		var track = new Track();

		if (!sys.FileSystem.exists(path)) {
			throw 'file does not exist $path';
		}

		var content = sys.io.File.getContent(path);
		track.loadRaw(content);

		return track;
	}

	public static function current() : Null<Track> {
		var folder = getFolder();

		dbg(c('looking in $folder for tracks'));

		if (!sys.FileSystem.exists(folder)) {
			dbg(c('$folder does not exist'));
			return null;
		}

		var active = [];
		for (subfolder in sys.FileSystem.readDirectory(folder)) {
			var group = haxe.io.Path.join([folder, subfolder]);
			if (sys.FileSystem.isDirectory(group)) {
				dbg(c('looking in $group for tracks'));
				for (file in sys.FileSystem.readDirectory(group)) {
					if (file.split(" ").length == 1) {
						dbg(c('found active tracking $file'));
						active.push(haxe.io.Path.join([group, file]));
					}
				}
			}
		}

		if (active.length > 1) {
			throw 'too many active! $active';
		}

		if (active.length == 1) return load(active[0]);
		else return null;
	}

	////////////////////////////////////////

	public function toFileContents() : String {
		var text = "";
		text += TrackFeature.Start + " " + DateTools.format(start, DATE_FORMAT) + "\n";
		if (end != null)
			text += TrackFeature.End + " " + DateTools.format(end, DATE_FORMAT) + "\n";
		if (annotation != null && annotation.length > 0)
			text += TrackFeature.Annotation + " " + annotation + "\n";
		if (tags != null && tags.length > 0)
			text += TrackFeature.Tag + " " + tags.join(",") + "\n";
		return text;

	}

	public function toString() : String {
		var stop = if (end == null) Date.now() else end;
		var duration = dur(sn.ds.DateTools.between(stop, start) * 60); // minutes

		var tags = if (tags.length > 0) " " + tgs(this.tags) else "";
		var annotation = if (annotation != null) " " + ann(annotation) else "";

		return '$duration ${paint("minutes", White)}$tags$annotation';
		return "";
	}

	public function format(fString:String, ?raw:Bool = false) : String {
		var regex = new EReg("\\%([A-Za-z]+)\\%", "g");

		while(regex.match(fString)) {
			var value:String = switch (regex.matched(1)) {
				case "duration":
					var stop = if (end == null) Date.now() else end;
					if (raw) '${sn.ds.DateTools.between(stop, start) * 60}';
					else dur(sn.ds.DateTools.between(stop, start) * 60);

				case "start":
						DateTools.format(start, Config.time_format);

				case "date":
					DateTools.format(start, Config.date_format);
			
				case "end":
					if (end == null) "..." else DateTools.format(end, Config.time_format);

				case "annotation":
					if (annotation != null) " " + ann(annotation) else "";

				case "tags":
					if (tags.length > 0) " " + tgs(this.tags) else "";

				default: "";
			}

			fString = fString.replace(regex.matched(0), value);
		}

		return fString;
	}

	public function getPath() : String {
		if (start == null) throw('entry has no start!');

		return haxe.io.Path.join([
			getFolder(),
			DateTools.format(start, SUB_FOLDER_FORMAT),
			getFileName()
		]);
	}

	public function getFileName() : String {
		var fileName = DateTools.format(start, DATE_FORMAT);
		if (end != null) fileName += " " + DateTools.format(end, DATE_FORMAT);
		return fileName;
	}

	public function hasTag(tag:String) : Bool {
		for (t in tags) {
			var parts = t.split("/");
			for (i in 1 ... parts.length + 1) {
				var bt = parts.slice(0,i).join("/");
				if(tag == bt) return true;
			}
		}
		return false;
	}

	public function save() {
		var path = getPath();
		dbg(c('$path'));
		sn.Fs.mkdir(haxe.io.Path.directory(path));
		sys.io.File.saveContent(path, toFileContents());
	}

	private function set(feature:TrackFeature, value:String) {
		switch(feature) {
			case Start:
				start = parseDate(value);

			case End:
				end = parseDate(value);

			case Tag:
				tags = value.split(",");

			case Annotation:
				annotation = value;
		}
	}

	public function remove() {
		var path = getPath();
		if (!sys.FileSystem.exists(path)) throw 'trying to delet $path but does not exist';
		sys.FileSystem.deleteFile(path);
	}

	inline private function parseDate(string:String) : Date {
		var date = new Date(
			Std.parseInt(string.substring(0,4)),
			Std.parseInt(string.substring(4,6)) - 1,
			Std.parseInt(string.substring(6,8)),
			Std.parseInt(string.substring(8,10)),
			Std.parseInt(string.substring(10,12)),
			Std.parseInt(string.substring(12,14))
		);
		return date;
	}

	private function loadRaw(data:String) {
		var components = TrackFeature.Features.copy();
		for (line in sn.ds.StringTools.lines(data)) {
			var found = false;

			for (comp in components) {
				if (comp == line.substring(0, (comp + "").length)) {
					found = true;
					// + 1 removes the separator, this case the space between
					// the id and the value
					set(comp, line.substring((comp + "").length + 1));
					components.remove(comp);
					break;
				}
			}

			if (!found) {
				wrn(c('unknown line $line found when loading data'));
			}
		}
	}
}
