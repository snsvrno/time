package;

class Main extends tui.Script {
	public static function main() new Main();

	override function setup() {
		// connecting the terminal outputs
		tui.Out.addPath(Debug(debug));
		tui.Out.addPath(Warning(warning));
		tui.Out.addPath(Error(error));

		name = "time";
		version = ds.Version.fromGit();
		description = Strings.general.app_description;

		defaultCommand = "status";

		addCommands(
			new tui.defaults.Config(Config),
			new commands.Cancel(),
			new commands.Edit(),
			new commands.List(),
			new commands.Resume(),
			new commands.Start(),
			new commands.Status(),
			new commands.Stop()
		);
	}

	override function init() {
		Config.load();
	}

	///////////////////

	private function debug(text:String) {
		Sys.print(paint(" DEBUG ", White, Blue, Bold) + " ");
		Sys.println(paintPreserve(text, Blue, FGBright));
	}

	private function warning(text:String) {
		Sys.print(paint(" WARNING ", White, Yellow, Bold) + " ");
		Sys.println(paintPreserve(text, Yellow, FGBright));
	}

	private function error(e:error.Error) {
		Sys.print(paint(" ERROR ", White, Red, Bold) + " ");
		Sys.println(paintPreserve(e.toString(), Red, FGBright));
	}
}
